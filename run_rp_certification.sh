#!/bin/sh


run_client()
{
  echo "running the client and forwarding stdout and sterr to $out"
  ./ksa-rp-client --alias ksa-rp \
  --clientid bc680915-bbd3-45d7-b3c6-2716f4d178ed \
  --transportCert="./model_bank/transport.crt" \
  --transportKey="./model_bank/transport.key" \
  --signingKey="./model_bank/signing.key" \
  --encryptionKey="./model_bank/encryption.key" >$out 2>%1

}


## declare an array variable
declare -a arr=("fapi1-advanced-final-client-test"
 "fapi1-advanced-final-client-test-encrypted-idtoken"
 "fapi1-advanced-final-client-test-encrypted-idtoken-usingrsa15"
 "fapi1-advanced-final-client-test-invalid-shash"
 "fapi1-advanced-final-client-test-invalid-chash"
 "fapi1-advanced-final-client-test-invalid-nonce"
 "fapi1-advanced-final-client-test-invalid-iss"
 "fapi1-advanced-final-client-test-invalid-aud"
 "fapi1-advanced-final-client-test-invalid-secondary-aud"
 "fapi1-advanced-final-client-test-invalid-signature"
 "fapi1-advanced-final-client-test-invalid-null-alg"
 "fapi1-advanced-final-client-test-invalid-alternate-alg"
 "fapi1-advanced-final-client-test-invalid-expired-exp"
 "fapi1-advanced-final-client-test-invalid-missing-exp"
 "fapi1-advanced-final-client-test-iat-is-week-in-past"
 "fapi1-advanced-final-client-test-invalid-missing-aud"
 "fapi1-advanced-final-client-test-invalid-missing-iss"
 "fapi1-advanced-final-client-test-invalid-missing-nonce"
 "fapi1-advanced-final-client-test-invalid-missing-shash"
 "fapi1-advanced-final-client-test-valid-aud-as-array"
 "fapi1-advanced-final-client-test-no-scope-in-token-endpoint-response"
 "fapi1-advanced-final-client-test-missing-athash"
 "fapi1-advanced-final-client-refresh-token-test"
 )

## now loop through the above array
for i in "${arr[@]}"
do
   echo "please start test $i and hit enter"
   out=$i.log
   read -r dummy
   run_client
done