package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/dvsekhvalnov/jose2go/base64url"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwe"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jws"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strings"
	"time"
)

type TokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	IDToken      string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
}

type ConsentResponse struct {
	Data struct {
		Permissions             []string  `json:"Permissions"`
		TransactionFromDateTime time.Time `json:"TransactionFromDateTime"`
		TransactionToDateTime   time.Time `json:"TransactionToDateTime"`
		ExpirationDateTime      time.Time `json:"ExpirationDateTime"`
		ConsentID               string    `json:"ConsentId"`
		Status                  string    `json:"Status"`
		CreationDateTime        time.Time `json:"CreationDateTime"`
		StatusUpdateDateTime    time.Time `json:"StatusUpdateDateTime"`
	} `json:"Data"`
	Links struct {
		Self string `json:"Self"`
	} `json:"Links"`
	Meta struct {
		TotalPages      int       `json:"TotalPages"`
		TotalRecords    int       `json:"TotalRecords"`
		RequestDateTime time.Time `json:"RequestDateTime"`
	} `json:"Meta"`
}

type PARResponse struct {
	RequestURI string `json:"request_uri"`
	ExpiresIn  int    `json:"expires_in"`
}

func main() {

	var local bool
	var testAlias string
	var transportPem string
	var transportKey string
	var signingKey string
	var encryptionKey string
	var privateKeyAuth bool
	var serverBaseUrl, serverBaseMtlsUrl string
	var clientid string
	var redirectUri string

	flag.BoolVar(&local, "local", false, "is the conformance suite running locally?")
	flag.StringVar(&testAlias, "alias", "", "Alias of the RP test")
	flag.StringVar(&transportPem, "transportCert", "", "Transport cert in PEM format")
	flag.StringVar(&transportKey, "transportKey", "", "Transport key in PEM format")
	flag.StringVar(&signingKey, "signingKey", "", "Signing key in PEM format")
	flag.StringVar(&encryptionKey, "encryptionKey", "", "Encryption key in PEM format")
	flag.BoolVar(&privateKeyAuth, "privateKeyAuth", false, "The client auth is private key? (if false, mtls will be used)")
	flag.StringVar(&serverBaseUrl, "serverBaseUrl", "https://www.certification.openid.net", "Conformance suite test path")
	flag.StringVar(&serverBaseMtlsUrl, "serverBaseMtlsUrl", "", "Conformance suite test path")
	flag.StringVar(&clientid, "clientid", "", "the clientid of the client")
	flag.StringVar(&redirectUri, "redirectUri", "https://localhost.emobix.co.uk:8443/test/a/ksa-ob/callback", "The redirect uri to be used by the client")
	flag.Parse()
	if testAlias == "" || transportPem == "" || transportKey == "" || signingKey == "" {
		fmt.Println("Usage: ksa-rp-client")
		flag.PrintDefaults()
		os.Exit(1)
	}

	cert, err := tls.LoadX509KeyPair(transportPem, transportKey)

	if err != nil {
		fmt.Println("Error: " + err.Error())
		fmt.Println("")
		fmt.Println("Usage: ksa-rp-client")
		flag.PrintDefaults()
		os.Exit(1)
	}

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	ResolveClientCert := func(*tls.CertificateRequestInfo) (*tls.Certificate, error) {
		return &cert, nil
	}

	clientMtls := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				Renegotiation:        tls.RenegotiateOnceAsClient,
				InsecureSkipVerify:   true,
				GetClientCertificate: ResolveClientCert,
			},
		},
	}

	if local {
		serverBaseUrl = "https://localhost.emobix.co.uk"
		serverBaseMtlsUrl = "https://mtls-localhost.emobix.co.uk"
	}

	if !strings.HasSuffix(serverBaseUrl, "/") {
		serverBaseUrl = serverBaseUrl + "/"
	}

	if serverBaseMtlsUrl == "" {
		serverBaseMtlsUrl = serverBaseUrl
	}

	if !strings.HasSuffix(serverBaseMtlsUrl, "/") {
		serverBaseMtlsUrl = serverBaseMtlsUrl + "/"
	}

	consentRequestEndpoint := serverBaseMtlsUrl + "test-mtls/a/" + testAlias + "/open-banking/v1.1/account-requests"
	accountEndpoint := serverBaseMtlsUrl + "test-mtls/a/" + testAlias + "/open-banking/v1.1/accounts"

	fmt.Println("Consent Request endpoint : " + consentRequestEndpoint)
	pkceCode, err := generate()
	if err != nil {
		panic(err)
	}
	pkceChallenge := challenge(pkceCode)

	signingKeyFile, err := os.ReadFile(signingKey)
	privateSignKey, err := jwk.ParseKey([]byte(signingKeyFile), jwk.WithPEM(true))
	privateSignKey.Set(jwk.KeyIDKey, "sig1")

	encryptionKeyFile, err := os.ReadFile(encryptionKey)
	privateEncKey, err := jwk.ParseKey([]byte(encryptionKeyFile), jwk.WithPEM(true))

	//******************************************************************
	//1. make the call do discovery
	fmt.Println("calling discovery")
	discoveryResp, err := client.Get(serverBaseUrl + "test/a/" + testAlias + "/.well-known/openid-configuration")
	if err != nil {
		panic(err)
	}
	discoveryStr, err := ioutil.ReadAll(discoveryResp.Body)
	if err != nil {
		panic(err)
	}
	discoveryResp.Body.Close()
	discovery := map[string]interface{}{}
	json.Unmarshal([]byte(discoveryStr), &discovery)

	fmt.Printf("Discovery response %s \n", discoveryStr)

	mtlsEndpoints := map[string]interface{}{}
	mtlsEndpoints = discovery["mtls_endpoint_aliases"].(map[string]interface{})

	token_endpoint := mtlsEndpoints["token_endpoint"].(string)
	par_endpoint := mtlsEndpoints["pushed_authorization_request_endpoint"].(string)
	authorization_endpoint := discovery["authorization_endpoint"].(string)

	//******************************************************************
	//2. make the call to JWKS
	fmt.Println("Calling JWKS")
	jwkResp, err := client.Get(discovery["jwks_uri"].(string))
	if err != nil {
		panic(err)
	}

	jwkStr, err := ioutil.ReadAll(jwkResp.Body)
	if err != nil {
		panic(err)
	}
	jwkResp.Body.Close()

	keySet, err := jwk.Parse(jwkStr)
	if err != nil {
		panic(err)
	}
	fmt.Println("loaded : %s", keySet)

	//******************************************************************
	//3. client credential to create a consent
	fmt.Println("client_credential call")

	clientCredentialBody := url.Values{}
	clientCredentialBody.Set("grant_type", "client_credentials")
	clientCredentialBody.Set("scope", "accounts openid")
	clientCredentialBody.Set("client_id", clientid)
	if privateKeyAuth {
		clientCredentialBody.Set("client_assertion", CreateAssertion(clientid, token_endpoint, privateSignKey))
		clientCredentialBody.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	}

	clientCredTokenRequest, err := http.NewRequest("POST", token_endpoint, strings.NewReader(clientCredentialBody.Encode()))
	if err != nil {
		panic(err)
	}

	clientCredTokenRequest.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	clientCredTokenResponse, err := clientMtls.Do(clientCredTokenRequest)
	if err != nil {
		panic(err)
	}

	clientCredentialResponse := &TokenResponse{}

	clientCredentialResponseStr, err := ioutil.ReadAll(clientCredTokenResponse.Body)
	if err != nil {
		panic(err)
	}
	clientCredTokenResponse.Body.Close()
	derr := json.Unmarshal(clientCredentialResponseStr, clientCredentialResponse)
	if derr != nil {
		panic(derr)
	}

	fmt.Printf("Client cred token: %s \n", clientCredentialResponse.AccessToken)

	//******************************************************************
	//4. create a consent
	fmt.Println("creating the consent")

	body := []byte(`{
    "Data": {
        "Permissions": [ 
            "ReadAccountsBasic",
         ],
        "TransactionFromDateTime": "2016-01-01T10:40:00+02:00",
        "TransactionToDateTime": "2025-12-31T10:40:00+02:00"
    }
}
`)

	consentRequest, err := http.NewRequest("POST", consentRequestEndpoint, bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	consentRequest.Header.Add("Content-Type", "application/json")
	consentRequest.Header.Add("Authorization", "Bearer "+clientCredentialResponse.AccessToken)

	consentResponse, err := clientMtls.Do(consentRequest)
	if err != nil {
		panic(err)
	}

	consent := &ConsentResponse{}
	consentResponseStr, err := ioutil.ReadAll(consentResponse.Body)
	if err != nil {
		panic(err)
	}
	consentResponse.Body.Close()
	fmt.Println("Consent Response : " + string(consentResponseStr))
	derr = json.Unmarshal(consentResponseStr, consent)
	if derr != nil {
		panic(derr)
	}

	fmt.Printf("Consent id : %s \n", consent.Data.ConsentID)

	//******************************************************************
	//5. create a request object
	fmt.Println("Creating Request Object")
	state := RandStringRunes(40)
	nonce := RandStringRunes(20)

	tok, err := jwt.NewBuilder().
		Audience([]string{discovery["issuer"].(string)}).
		Issuer(clientid).
		IssuedAt(time.Now()).
		NotBefore(time.Now().Add(time.Second*-10)).
		Expiration(time.Now().Add(time.Second*300)).
		Claim("client_id", clientid).
		Claim("response_type", "code id_token").
		Claim("code_challenge_method", "S256").
		Claim("code_challenge", pkceChallenge).
		Claim("redirect_uri", redirectUri).
		Claim("scope", "accounts:"+consent.Data.ConsentID+" openid").
		Claim("state", state).
		Claim("nonce", nonce).
		Build()
	if err != nil {
		panic(err)
	}

	hdrs := jws.NewHeaders()
	hdrs.Set("typ", "oauth-authz-req+jwt")

	signed, err := jwt.Sign(tok, jwt.WithKey(jwa.PS256, privateSignKey, jws.WithProtectedHeaders(hdrs)))
	if err != nil {
		panic(err)
	}

	//******************************************************************
	//6. create the par request
	fmt.Println("submitting to par endpoint")

	parBody := url.Values{}
	parBody.Set("request", string(signed))
	parBody.Set("client_id", clientid)
	if privateKeyAuth {
		parBody.Set("client_assertion", CreateAssertion(clientid, discovery["issuer"].(string), privateSignKey))
		parBody.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	}
	parRequest, err := http.NewRequest("POST", par_endpoint, strings.NewReader(parBody.Encode()))
	if err != nil {
		panic(err)
	}

	parRequest.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	parResponse, err := clientMtls.Do(parRequest)
	if err != nil {
		panic(err)
	}

	par := &PARResponse{}
	parResponseStr, err := ioutil.ReadAll(parResponse.Body)
	if err != nil {
		panic(err)
	}
	parResponse.Body.Close()
	derr = json.Unmarshal(parResponseStr, par)

	if derr != nil {
		panic(derr)
	}
	fmt.Println("request uri created: " + par.RequestURI)

	//******************************************************************
	//7. make the authorization call
	fmt.Println("submitting to authorization endpoint")

	authorizeRequest, err := http.NewRequest("GET", authorization_endpoint, nil)
	if err != nil {
		panic(err)
	}

	authorizeParams := authorizeRequest.URL.Query()
	authorizeParams.Set("request_uri", par.RequestURI)
	authorizeParams.Set("client_id", clientid)

	authorizeRequest.URL.RawQuery = authorizeParams.Encode()

	fmt.Println(authorizeRequest.URL)

	authorizeResponse, err := client.Do(authorizeRequest)

	if err != nil {
		panic(err)
	}
	fmt.Println("processing to authorization redirect")
	if !strings.Contains(authorizeResponse.Status, "303") {
		panic("Authorize didn't redirect")
	}
	authorizeHeader := authorizeResponse.Header
	newLocation := authorizeHeader["Location"][0]
	redirect, err := url.Parse(newLocation)
	authorizationParams := redirect.Query()
	fragments, _ := url.ParseQuery(redirect.Fragment)

	code := authorizationParams.Get("code")
	if code == "" {
		code = fragments.Get("code")
	}

	stateRedirect := authorizationParams.Get("state")
	if stateRedirect == "" {
		stateRedirect = fragments.Get("state")
	}

	id_token := authorizationParams.Get("id_token")
	if id_token == "" {
		id_token = fragments.Get("id_token")
	}
	id_token_encrypted := false
	if strings.Count(id_token, ".") > 2 {
		id_token_encrypted = true
	}

	fmt.Printf("    authorization code: %s \n    state: %s\n    id_token: %s\n    id_token_encrypted: %t\n", code, state, id_token, id_token_encrypted)

	authorizeResponse.Body.Close()

	//******************************************************************
	//9. validating the authorization redirect

	var idToken jwt.Token

	iatValidator := jwt.ValidatorFunc(func(_ context.Context, t jwt.Token) jwt.ValidationError {
		now := time.Now()
		diff := now.Sub(t.IssuedAt())
		inRange := (diff >= time.Minute*-5 && diff <= time.Second*5)
		if !inRange {
			return jwt.NewValidationError(errors.New(`invalid iat`))
		}
		return nil
	})

	idTokenValidators := []jwt.ParseOption{jwt.WithKeySet(keySet),
		jwt.WithAcceptableSkew(5 * time.Second),
		jwt.WithValidate(true),
		jwt.WithValidator(iatValidator),
		jwt.WithValidator(jwt.IsRequired("iss")),
		jwt.WithValidator(jwt.IsRequired("exp")),
		jwt.WithValidator(jwt.IsRequired("aud")),
		jwt.WithValidator(jwt.IsRequired("nonce")),
		jwt.WithValidator(jwt.IsRequired("s_hash")),
		jwt.WithValidator(jwt.IsRequired("c_hash")),
	}
	if id_token_encrypted {
		decryptedToken, err := jwe.Decrypt([]byte(id_token), jwe.WithKey(jwa.RSA_OAEP, privateEncKey))
		if err != nil {
			panic(err)
		}
		fmt.Printf("Decrypted token : %q \n", decryptedToken)
		idToken, err = jwt.Parse([]byte(decryptedToken), idTokenValidators...)

	} else {
		idToken, err = jwt.Parse([]byte(id_token), idTokenValidators...)
		if err != nil {
			fmt.Printf("id token invalid %s\n", err)
			return
		}
	}
	nonceToken, _ := idToken.Get("nonce")
	cHashToken, _ := idToken.Get("c_hash")
	sHashToken, _ := idToken.Get("s_hash")
	issToken, _ := idToken.Get("iss")
	audToken, _ := idToken.Get("aud")
	azpToken, _ := idToken.Get("azp")

	if discovery["issuer"].(string) != issToken {
		panic("iss on id token invalid")
	}

	if state != stateRedirect {
		panic("State on redirect invalid")
	}

	if nonce != nonceToken.(string) {
		panic("Nonce on redirect invalid")
	}

	authCodeString := calcHash(code)
	if authCodeString != cHashToken {
		panic("c_hash does not match calculated auth code hash")
	}

	stateHashString := calcHash(state)
	if stateHashString != sHashToken {
		panic("s_hash does not match calculated state hash")
	}

	if reflect.TypeOf(audToken).Kind() == reflect.String {
		if audToken.(string) != clientid {
			panic("audience on the token does not match the client id")
		}
	} else {
		aud := audToken.([]string)
		found := false
		for _, s := range aud {
			if s == clientid {
				found = true
			}
		}
		if !found {
			panic("client not found on audience list")
		}
		if len(aud) > 1 && (azpToken == nil || azpToken == "") {
			panic("azp not found")
		}

	}
	//******************************************************************
	//10. make the call to token endpoint

	fmt.Println("submitting to token endpoint")

	authCodeTokenBody := url.Values{}
	authCodeTokenBody.Set("code", code)
	authCodeTokenBody.Set("grant_type", "authorization_code")

	authCodeTokenBody.Set("client_id", clientid)
	authCodeTokenBody.Set("code_verifier", pkceCode)
	authCodeTokenBody.Set("redirect_uri", redirectUri)

	if privateKeyAuth {
		authCodeTokenBody.Set("client_assertion", CreateAssertion(clientid, token_endpoint, privateSignKey))
		authCodeTokenBody.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	}

	authCodeTokenRequest, err := http.NewRequest("POST", token_endpoint, strings.NewReader(authCodeTokenBody.Encode()))
	if err != nil {
		panic(err)
	}

	authCodeTokenRequest.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	authCodeTokenResponse, err := clientMtls.Do(authCodeTokenRequest)
	if err != nil {
		panic(err)
	}

	authCodeToken := &TokenResponse{}
	authCodeTokenStr, err := ioutil.ReadAll(authCodeTokenResponse.Body)
	if err != nil {
		panic(err)
	}
	authCodeTokenResponse.Body.Close()
	derr = json.Unmarshal(authCodeTokenStr, authCodeToken)

	if derr != nil {
		panic(derr)
	}
	fmt.Println("token : " + authCodeToken.AccessToken)

	//******************************************************************
	//11. make the call to account endpoint

	accountResponseStatus := callAccountEndpoint(err, accountEndpoint, authCodeToken, clientMtls)

	for strings.Contains(accountResponseStatus, "401") {
		fmt.Println("Running the refresh token flow")

		//******************************************************************
		//12. make the call to token endpoint to refresh token
		err, refreshToken := refreshToken(authCodeToken, clientid, redirectUri,
			token_endpoint, clientMtls, privateKeyAuth, &privateSignKey)
		fmt.Println("token : " + refreshToken.AccessToken)

		//******************************************************************
		//13. call account endpoint again

		accountResponseStatus = callAccountEndpoint(err, accountEndpoint, refreshToken, clientMtls)

		authCodeToken = refreshToken
	}
}

func refreshToken(authCodeToken *TokenResponse, clientid string,
	redirectUri string, token_endpoint string,
	clientMtls *http.Client, privateKeyAuth bool,
	privateSignKey *jwk.Key) (error, *TokenResponse) {
	refreshTokenBody := url.Values{}
	refreshTokenBody.Set("refresh_token", authCodeToken.RefreshToken)
	refreshTokenBody.Set("grant_type", "refresh_token")
	refreshTokenBody.Set("client_id", clientid)
	refreshTokenBody.Set("redirect_uri", redirectUri)
	if privateKeyAuth {
		refreshTokenBody.Set("client_assertion", CreateAssertion(clientid, token_endpoint, *privateSignKey))
		refreshTokenBody.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	}

	refreshTokenRequest, err := http.NewRequest("POST", token_endpoint, strings.NewReader(refreshTokenBody.Encode()))
	if err != nil {
		panic(err)
	}

	refreshTokenRequest.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	refreshTokenResponse, err := clientMtls.Do(refreshTokenRequest)
	if err != nil {
		panic(err)
	}

	refreshToken := &TokenResponse{}
	refreshTokenStr, err := ioutil.ReadAll(refreshTokenResponse.Body)
	if err != nil {
		panic(err)
	}
	refreshTokenResponse.Body.Close()
	derr := json.Unmarshal(refreshTokenStr, refreshToken)

	if derr != nil {
		panic(derr)
	}
	return err, refreshToken
}

func callAccountEndpoint(err error, accountEndpoint string, authCodeToken *TokenResponse, clientMtls *http.Client) string {
	fmt.Println("Calling account endpoint")
	accountRequest, err := http.NewRequest("GET", accountEndpoint, nil)
	if err != nil {
		panic(err)
	}

	accountRequest.Header.Add("Authorization", "Bearer "+authCodeToken.AccessToken)

	accountResponse, err := clientMtls.Do(accountRequest)
	if err != nil {
		panic(err)
	}

	accountResponseStr, err := ioutil.ReadAll(accountResponse.Body)

	fmt.Printf(" account response (%s): \n      %q\n", accountResponse.Status, accountResponseStr)
	accountResponseStatus := accountResponse.Status
	return accountResponseStatus
}

func calcHash(str string) string {
	h := sha256.New()
	h.Write([]byte(str))
	strHash := h.Sum(nil)
	strHash = strHash[0 : len(strHash)/2]

	encoded := base64url.Encode(strHash)
	return encoded
}

var letterRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func CreateAssertion(clientid, serverEndpoint string, key jwk.Key) string {

	tok, err := jwt.NewBuilder().
		Issuer(clientid).
		Audience([]string{serverEndpoint}).
		Subject(clientid).
		IssuedAt(time.Now().Add(time.Second*-5)).
		Expiration(time.Now().Add(time.Minute*2)).
		Claim("jti", RandStringRunes(10)).
		Build()
	if err != nil {
		panic(err)
	}
	h := jws.NewHeaders()
	h.Set("kid", "sig1")

	serialized, err := jwt.Sign(tok, jwt.WithKey(jwa.PS256, key), jwt.WithSignOption(jws.WithHeaders(h)))
	if err != nil {
		panic(err)
	}
	return string(serialized)
}
