with import <nixpkgs> {};

mkShell {
    name = "oidf-ksa-rp";
    packages = [
        figlet
        go

    ];
    shellHook = ''
        figlet ":KSA Demo RP:"
        unset GOROOT
        echo "check README.md for details"
        echo "building the client for you...."
        go mod tidy
        go build .
    '';
}